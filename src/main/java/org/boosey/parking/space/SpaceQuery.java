package org.boosey.parking.space;

import javax.inject.Singleton;

import io.grpc.stub.StreamObserver;

@Singleton
public class SpaceQuery extends SpaceQueryGrpc.SpaceQueryImplBase {

    @Override
    public void listSpaces(SpaceListRequest request, StreamObserver<SpaceListReply> responsObserver) {
        responsObserver.onNext(SpaceListReply.newBuilder().setTempJSONString("The RPC has compete successfully").build());
        responsObserver.onCompleted();
    }

}